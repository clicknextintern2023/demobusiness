﻿using DemoBusinessApp.Constant;
using System.ComponentModel.DataAnnotations;

namespace DemoBusinessApp.ViewModels
{
    public class ContactInfoPost
    {
        public int ContactInfoId { get; set; }

        [Required]
        public IdNumberType IdNumberType { get; set; }

        [StringLength(13, MinimumLength = 13, ErrorMessage = "IdNumber must be exactly 13 characters")]
        public string IdNumber { get; set; } = null!;

        public BusinessType BusinessType { get; set; }

        [Required]
        [StringLength(100)]
        public string BusinessName { get; set; } = null!;

        [Required]
        public int BusinessBranchId { get; set; }
        public List<AddressPost> Addresses { get; set; } = null!;
    }
}
