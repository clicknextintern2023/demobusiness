﻿using DemoBusiness.Interfaces;
using DemoBusinessApp.Constant;
using DemoBusinessApp.Filter;
using DemoBusinessApp.Models;
using DemoBusinessApp.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace DemoBusiness.Repository
{
    public class ContactInforepository : IContactInfoRepository
    {
        private readonly DemoBusinessAPIContext _context;
        public ContactInforepository(DemoBusinessAPIContext context)
        {
            _context = context;
        }
        public ResultAPI<List<ContactInfoResult>> GetContactInfos(GetContactInfosFilter getContactInfosFilter)
        {
            try
            {
                var contactInfos = _context.ContactInfos
                                        .Join(_context.BusinessBranches, contact => contact.BusinessBranchId, contactBranch => contactBranch.BusinessBranchId, (contact, contactBranch) => new { contact, contactBranch })
                                        .Where(c => string.IsNullOrEmpty(getContactInfosFilter.IdNumber) || c.contact.IdNumber == getContactInfosFilter.IdNumber)
                                        .Where(c => string.IsNullOrEmpty(getContactInfosFilter.BusinessName) || c.contact.BusinessName == getContactInfosFilter.BusinessName)
                                        .Where(c => string.IsNullOrEmpty(getContactInfosFilter.AddressInfo) || c.contact.Addresses.Any(a => a.AddressInfo == getContactInfosFilter.AddressInfo))
                                        .Where(c => !getContactInfosFilter.StartDate.HasValue || c.contact.CreateDate >= getContactInfosFilter.StartDate.Value)
                                        .Where(c => !getContactInfosFilter.EndDate.HasValue || c.contact.CreateDate <= getContactInfosFilter.EndDate.Value)
                                        .Select(c => new ContactInfoResult
                                        {
                                            ContactInfoId = c.contact.ContactInfoId,
                                            IdNumberType = (IdNumberType)c.contact.IdNumberType,
                                            IdNumberTypeName = ((IdNumberType)c.contact.IdNumberType).ToString(),
                                            IdNumber = c.contact.IdNumber,
                                            BusinessType = (BusinessType)c.contact.BusinessType,
                                            BusinessTypeName = ((BusinessType)c.contact.BusinessType).ToString(),
                                            BusinessName = c.contact.BusinessName,
                                            BusinessBranchId = c.contact.BusinessBranchId,
                                            BusinessBranchName = c.contactBranch.BusinessBranchType
                                        }).AsNoTracking().ToDictionary(c => c.ContactInfoId, c => c);

                if (contactInfos.Count == 0)
                    return new ResultAPI<List<ContactInfoResult>> { Message = "No Contact Found", StatusCode = 200 };

                var contactInfoIds = contactInfos.Select(c => c.Value.ContactInfoId);

                var addresses = _context.Addresses.Where(c => contactInfoIds.Contains(c.ContactInfoId))
                                                  .Select(c => new AddressResult
                                                  {
                                                      AddressId = c.AddressId,
                                                      AddressType = (AddressType)c.AddressType,
                                                      AddressTypeName = ((AddressType)c.AddressType).ToString(),
                                                      AddressName = c.AddressName,
                                                      AddressInfo = c.AddressInfo,
                                                      AddressSubdistrict = c.AddressSubdistrict,
                                                      AddressDistrict = c.AddressDistrict,
                                                      AddressProvince = c.AddressProvince,
                                                      AddressPostalCode = c.AddressPostalCode,
                                                      ContactInfoId = c.ContactInfoId,
                                                  });

                foreach (var address in addresses)
                {
                    contactInfos[address.ContactInfoId].Addresses.Add(address);
                }

                return new ResultAPI<List<ContactInfoResult>> { Message = "Success", StatusCode = 200, Data = contactInfos.Values.ToList() };
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
                return new ResultAPI<List<ContactInfoResult>> { Message = ex.Message, StatusCode = 500 };
            }
        }

        public ResultAPI<ContactInfoResult> GetContactInfoById(GetContactInfoFilter getContactInfoFilter)
        {
            try
            {
                var contactInfo = _context.ContactInfos
                    .Where(c => c.ContactInfoId == getContactInfoFilter.ContactInfoId)
                    .Select(c => new ContactInfoResult
                    {
                        ContactInfoId = c.ContactInfoId,
                        IdNumberType = (IdNumberType)c.IdNumberType,
                        IdNumberTypeName = ((IdNumberType)c.IdNumberType).ToString(),
                        IdNumber = c.IdNumber,
                        BusinessType = (BusinessType)c.BusinessType,
                        BusinessTypeName = ((BusinessType)c.BusinessType).ToString(),
                        BusinessName = c.BusinessName,
                        BusinessBranchId = c.BusinessBranchId,
                    })
                    .Single();

                var addresses = _context.Addresses
                    .Where(a => a.ContactInfoId == getContactInfoFilter.ContactInfoId)
                    .Select(a => new AddressResult
                    {
                        AddressId = a.AddressId,
                        AddressType = (AddressType)a.AddressType,
                        AddressTypeName = ((AddressType)a.AddressType).ToString(),
                        AddressName = a.AddressName,
                        AddressInfo = a.AddressInfo,
                        AddressSubdistrict = a.AddressSubdistrict,
                        AddressDistrict = a.AddressDistrict,
                        AddressProvince = a.AddressProvince,
                        AddressPostalCode = a.AddressPostalCode,
                        ContactInfoId = a.ContactInfoId,
                    }).ToList();

                var businessBranch = _context.BusinessBranches.Where(a => a.BusinessBranchId == contactInfo.BusinessBranchId).Single();

                contactInfo.Addresses = addresses;
                contactInfo.BusinessBranchName = businessBranch.BusinessBranchType;

                return new ResultAPI<ContactInfoResult> { Message = "Success", StatusCode = 200, Data = contactInfo };
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
                return new ResultAPI<ContactInfoResult> { Message = ex.Message, StatusCode = 500 };
            }
        }

        public ResultAPI<int> DeleteContactInfoById(ContactInfoDelete deleteContactInfoFilter)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var contactInfo = _context.ContactInfos.Where(x => x.ContactInfoId == deleteContactInfoFilter.ContactInfoId).Single();
                    var contactInfoAddress = _context.Addresses.Where(a => a.ContactInfoId == deleteContactInfoFilter.ContactInfoId);
                    _context.RemoveRange(contactInfoAddress);
                    _context.ContactInfos.Remove(contactInfo);

                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultAPI<int> { Message = "Delete Success", StatusCode = 200, Data = deleteContactInfoFilter.ContactInfoId };
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                    dbContextTransaction?.Rollback();
                    return new ResultAPI<int> { Message = ex.Message, StatusCode = 500 };
                }
            }
        }

        public ResultAPI<int> AddContactInfo(ContactInfoPost contactInfoModel)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var newContactInfo = new ContactInfo
                    {
                        IdNumberType = (int)contactInfoModel.IdNumberType,
                        IdNumber = contactInfoModel.IdNumber,
                        BusinessType = (int)contactInfoModel.BusinessType,
                        BusinessName = contactInfoModel.BusinessName,
                        CreateDate = DateTime.Now,
                        BusinessBranchId = contactInfoModel.BusinessBranchId,
                    };
                    _context.ContactInfos.Add(newContactInfo);
                    _context.SaveChanges();

                    // Check if there is more or less than one register address
                    if (contactInfoModel.Addresses.Count(a => a.AddressType == AddressType.RegAddress) != 1)
                        return new ResultAPI<int> { Message = "Register address must have exactly 1 address", StatusCode = 500, Data = 0 };

                    foreach (var addressDto in contactInfoModel.Addresses)
                    {
                        var newAddress = new Address
                        {
                            AddressType = (int)addressDto.AddressType,
                            AddressName = addressDto.AddressName,
                            AddressInfo = addressDto.AddressInfo,
                            AddressSubdistrict = addressDto.AddressSubdistrict,
                            AddressDistrict = addressDto.AddressDistrict,
                            AddressProvince = addressDto.AddressProvince,
                            AddressPostalCode = addressDto.AddressPostalCode,
                            ContactInfoId = newContactInfo.ContactInfoId,
                        };
                        _context.Addresses.Add(newAddress);
                    }
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultAPI<int> { Message = "Add Success", StatusCode = 200, Data = newContactInfo.ContactInfoId };
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                    dbContextTransaction.Rollback();
                    return new ResultAPI<int> { Message = ex.Message, StatusCode = 500 };
                }
            }
        }

        public ResultAPI<int> UpdateContactInfo(ContactInfoPost contactInfoModel)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var newContactInfo = _context.ContactInfos.Single(c => c.ContactInfoId == contactInfoModel.ContactInfoId);
                    newContactInfo.IdNumberType = (int)contactInfoModel.IdNumberType;
                    newContactInfo.IdNumber = contactInfoModel.IdNumber;
                    newContactInfo.BusinessType = (int)contactInfoModel.BusinessType;
                    newContactInfo.BusinessName = contactInfoModel.BusinessName;
                    newContactInfo.UpdateDate = DateTime.Now;
                    newContactInfo.BusinessBranchId = contactInfoModel.BusinessBranchId;

                    if (contactInfoModel.Addresses != null && contactInfoModel.Addresses.Count(a => a.AddressType == AddressType.RegAddress) == 1) // กรณีมีที่อยู่และมีที่อยู่จดทะเบียน1ที่
                    {
                        var addressIds = contactInfoModel.Addresses.Select(a => a.AddressId).ToList(); // เก็บ list ของ id address ที่เข้ามาทั้งหมด
                        var unMatchAddress = _context.Addresses.Where(a => a.ContactInfoId == contactInfoModel.ContactInfoId && !addressIds.Contains(a.AddressId)).ToList();
                        _context.Addresses.RemoveRange(unMatchAddress);

                        foreach (var addressDto in contactInfoModel.Addresses)
                        {
                            if (addressDto.AddressId.HasValue) // กรณีไม่ใช่ที่อยุ่ที่เพิ่มขึ้นมาใหม่
                            {
                                var oldAddress = _context.Addresses.Single(a => a.AddressId == addressDto.AddressId);
                                oldAddress.AddressType = (int)addressDto.AddressType;
                                oldAddress.AddressName = addressDto.AddressName;
                                oldAddress.AddressInfo = addressDto.AddressInfo;
                                oldAddress.AddressSubdistrict = addressDto.AddressSubdistrict;
                                oldAddress.AddressDistrict = addressDto.AddressDistrict;
                                oldAddress.AddressProvince = addressDto.AddressProvince;
                                oldAddress.AddressPostalCode = addressDto.AddressPostalCode;
                                oldAddress.ContactInfoId = addressDto.ContactInfoId;
                            }
                            else // กรณีที่เพิ่มขึ้นมาใหม่
                            {
                                var newAddress = new Address
                                {
                                    AddressType = (int)addressDto.AddressType,
                                    AddressName = addressDto.AddressName,
                                    AddressInfo = addressDto.AddressInfo,
                                    AddressSubdistrict = addressDto.AddressSubdistrict,
                                    AddressDistrict = addressDto.AddressDistrict,
                                    AddressProvince = addressDto.AddressProvince,
                                    AddressPostalCode = addressDto.AddressPostalCode,
                                    ContactInfoId = contactInfoModel.ContactInfoId,
                                };
                                _context.Addresses.Add(newAddress);
                            }
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        return new ResultAPI<int> { Message = "Register address must have exactly 1 address", StatusCode = 500, Data = 0 };
                    }
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultAPI<int> { Message = "Update Success", StatusCode = 200, Data = contactInfoModel.ContactInfoId };
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                    dbContextTransaction.Rollback();
                    return new ResultAPI<int> { Message = ex.Message, StatusCode = 500 };
                }
            }
        }
    }
}
