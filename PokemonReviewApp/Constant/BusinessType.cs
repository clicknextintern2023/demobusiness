﻿namespace DemoBusinessApp.Constant
{
    public enum BusinessType
    {
        LegalEntity = 1, // นิติบุคคล
        Individual = 2, // บุคคลธรรมดา
    }
}
