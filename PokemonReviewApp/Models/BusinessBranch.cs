﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class BusinessBranch
    {
        public BusinessBranch()
        {
            ContactInfos = new HashSet<ContactInfo>();
        }

        public int BusinessBranchId { get; set; }
        public string BusinessBranchType { get; set; } = null!;

        public virtual ICollection<ContactInfo> ContactInfos { get; set; }
    }
}
