﻿using DemoBusinessApp.Constant;
using System.ComponentModel.DataAnnotations;

namespace DemoBusinessApp.ViewModels
{
    public class ContactInfoResult
    {
        public ContactInfoResult()
        {
            Addresses = new List<AddressResult>();
        }
        public int ContactInfoId { get; set; }
        public IdNumberType IdNumberType { get; set; }
        public string IdNumberTypeName { get; set; } = null!;
        public string IdNumber { get; set; } = null!;
        public BusinessType BusinessType { get; set; }
        public string BusinessTypeName { get; set; } = null!;
        public string BusinessName { get; set; } = null!;
        public int BusinessBranchId { get; set; }
        public string BusinessBranchName { get; set; } = null!;
        public List<AddressResult> Addresses { get; set; } = null!;
    }
}
