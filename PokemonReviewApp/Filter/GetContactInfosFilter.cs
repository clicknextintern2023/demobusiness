﻿namespace DemoBusinessApp.Filter
{
    public class GetContactInfosFilter
    {
        public string? IdNumber { get; set; }
        public string? BusinessName { get; set; }
        public string? AddressInfo { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
