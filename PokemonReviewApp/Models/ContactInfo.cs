﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class ContactInfo
    {
        public ContactInfo()
        {
            Addresses = new HashSet<Address>();
            BankAccounts = new HashSet<BankAccount>();
        }

        public int ContactInfoId { get; set; }
        public int IdNumberType { get; set; }
        public string IdNumber { get; set; } = null!;
        public int BusinessType { get; set; }
        public string BusinessName { get; set; } = null!;
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int BusinessBranchId { get; set; }

        public virtual BusinessBranch BusinessBranch { get; set; } = null!;
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
    }
}
