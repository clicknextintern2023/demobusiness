﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class BankAccount
    {
        public int BankAccountId { get; set; }
        public string? BankAccountNumber { get; set; }
        public string? BankAccountBranch { get; set; }
        public int AccountTypeId { get; set; }
        public int BankId { get; set; }
        public int ContactInfoId { get; set; }

        public virtual AccountType AccountType { get; set; } = null!;
        public virtual Bank Bank { get; set; } = null!;
        public virtual ContactInfo ContactInfo { get; set; } = null!;
    }
}
