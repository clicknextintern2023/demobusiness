﻿using DemoBusinessApp.Filter;
using DemoBusinessApp.ViewModels;

namespace DemoBusiness.Interfaces
{
    public interface IContactInfoRepository
    {
        ResultAPI<List<ContactInfoResult>> GetContactInfos(GetContactInfosFilter getContactInfosFilter);
        ResultAPI<ContactInfoResult> GetContactInfoById(GetContactInfoFilter getContactInfoFilter);
        ResultAPI<int> DeleteContactInfoById(ContactInfoDelete deleteContactInfoFilter);
        ResultAPI<int> AddContactInfo(ContactInfoPost contactInfoModel);
        ResultAPI<int> UpdateContactInfo(ContactInfoPost contactInfoModel);
    }
}
