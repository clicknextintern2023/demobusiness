﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class Bank
    {
        public Bank()
        {
            BankAccounts = new HashSet<BankAccount>();
        }

        public int BankId { get; set; }
        public string BankName { get; set; } = null!;

        public virtual ICollection<BankAccount> BankAccounts { get; set; }
    }
}
