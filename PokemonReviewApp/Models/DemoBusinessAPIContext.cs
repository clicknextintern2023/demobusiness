﻿using Microsoft.EntityFrameworkCore;

namespace DemoBusinessApp.Models
{
    public partial class DemoBusinessAPIContext : DbContext
    {
        public DemoBusinessAPIContext()
        {
        }

        public DemoBusinessAPIContext(DbContextOptions<DemoBusinessAPIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountType> AccountTypes { get; set; } = null!;
        public virtual DbSet<Address> Addresses { get; set; } = null!;
        public virtual DbSet<Bank> Banks { get; set; } = null!;
        public virtual DbSet<BankAccount> BankAccounts { get; set; } = null!;
        public virtual DbSet<BusinessBranch> BusinessBranches { get; set; } = null!;
        public virtual DbSet<ContactInfo> ContactInfos { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=NASAN-LAPTOP;Initial Catalog=DemoBusinessAPI;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountType>(entity =>
            {
                entity.ToTable("AccountType");

                entity.Property(e => e.AccountTypeName).HasMaxLength(50);
            });

            modelBuilder.Entity<Address>(entity =>
            {
                entity.ToTable("Address");

                entity.Property(e => e.AddressDistrict).HasMaxLength(100);

                entity.Property(e => e.AddressInfo).HasMaxLength(200);

                entity.Property(e => e.AddressName).HasMaxLength(100);

                entity.Property(e => e.AddressPostalCode).HasMaxLength(100);

                entity.Property(e => e.AddressProvince).HasMaxLength(100);

                entity.Property(e => e.AddressSubdistrict).HasMaxLength(100);

                entity.HasOne(d => d.ContactInfo)
                    .WithMany(p => p.Addresses)
                    .HasForeignKey(d => d.ContactInfoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Address_ContactInfo");
            });

            modelBuilder.Entity<Bank>(entity =>
            {
                entity.ToTable("Bank");

                entity.Property(e => e.BankName).HasMaxLength(100);
            });

            modelBuilder.Entity<BankAccount>(entity =>
            {
                entity.ToTable("BankAccount");

                entity.Property(e => e.BankAccountBranch).HasMaxLength(100);

                entity.Property(e => e.BankAccountNumber)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccountType)
                    .WithMany(p => p.BankAccounts)
                    .HasForeignKey(d => d.AccountTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BankAccount_AccountType");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.BankAccounts)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BankAccount_Bank");

                entity.HasOne(d => d.ContactInfo)
                    .WithMany(p => p.BankAccounts)
                    .HasForeignKey(d => d.ContactInfoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_BankAccount_ContactInfo");
            });

            modelBuilder.Entity<BusinessBranch>(entity =>
            {
                entity.ToTable("BusinessBranch");

                entity.Property(e => e.BusinessBranchType).HasMaxLength(50);
            });

            modelBuilder.Entity<ContactInfo>(entity =>
            {
                entity.ToTable("ContactInfo");

                entity.HasIndex(e => e.IdNumber, "Unique_IdNumber")
                    .IsUnique();

                entity.Property(e => e.BusinessName).HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.IdNumber).HasMaxLength(13);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.BusinessBranch)
                    .WithMany(p => p.ContactInfos)
                    .HasForeignKey(d => d.BusinessBranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContactInfo_BusinessBranch");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
