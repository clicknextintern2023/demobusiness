﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class AccountType
    {
        public AccountType()
        {
            BankAccounts = new HashSet<BankAccount>();
        }

        public int AccountTypeId { get; set; }
        public string AccountTypeName { get; set; } = null!;

        public virtual ICollection<BankAccount> BankAccounts { get; set; }
    }
}
