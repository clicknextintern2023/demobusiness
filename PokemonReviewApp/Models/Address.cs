﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class Address
    {
        public int AddressId { get; set; }
        public int AddressType { get; set; }
        public string? AddressName { get; set; }
        public string? AddressInfo { get; set; }
        public string? AddressSubdistrict { get; set; }
        public string? AddressDistrict { get; set; }
        public string? AddressProvince { get; set; }
        public string? AddressPostalCode { get; set; }
        public int ContactInfoId { get; set; }

        public virtual ContactInfo ContactInfo { get; set; } = null!;
    }
}
