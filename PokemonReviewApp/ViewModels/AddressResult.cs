﻿using DemoBusinessApp.Constant;

namespace DemoBusinessApp.ViewModels
{
    public class AddressResult
    {
        public int AddressId { get; set; } 
        public AddressType AddressType { get; set; }
        public string? AddressTypeName { get; set; } 
        public string? AddressName { get; set; }
        public string? AddressInfo { get; set; }
        public string? AddressSubdistrict { get; set; } 
        public string? AddressDistrict { get; set; } 
        public string? AddressProvince { get; set; } 
        public string? AddressPostalCode { get; set; } 
        public int ContactInfoId { get; set; }
    }
}
