﻿namespace DemoBusinessApp.Constant
{
    public enum AddressType
    {
        RegAddress = 1,
        ShipAddress = 2,
    }
}
