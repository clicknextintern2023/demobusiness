﻿using DemoBusiness.Interfaces;
using DemoBusinessApp.Filter;
using DemoBusinessApp.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DemoBusiness.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class ContactInfoController : Controller
    {
        private readonly IContactInfoRepository _contactInfoRepository;

        public ContactInfoController(IContactInfoRepository contactInfoRepository)
        {
            _contactInfoRepository = contactInfoRepository;
        }
        
        [HttpGet]
        public IActionResult GetContactInfos([FromQuery] GetContactInfosFilter filter)
        {
            var results = _contactInfoRepository.GetContactInfos(filter);
            return Ok(results);
        }

        [HttpGet]
        public IActionResult GetContactInfoById([FromQuery] GetContactInfoFilter getContactInfo)
        {
            var contactInfo = _contactInfoRepository.GetContactInfoById(getContactInfo);
            return Ok(contactInfo);
        }

        [HttpPost]
        public IActionResult DeleteContactInfo([FromBody] ContactInfoDelete deleteContactInfo) 
        {
            var deletedContactInfo = _contactInfoRepository.DeleteContactInfoById(deleteContactInfo);
            return Ok(deletedContactInfo);
        }

        [HttpPost]
        public IActionResult AddContactInfo([FromBody] ContactInfoPost contactInfoDto)
        {
            var addedContactInfo = _contactInfoRepository.AddContactInfo(contactInfoDto);
            return Ok(addedContactInfo);
        }

        [HttpPost]
        public IActionResult UpdateContactInfo([FromBody] ContactInfoPost updateContactInfoDto)
        {
            var updatedContactInfo = _contactInfoRepository.UpdateContactInfo(updateContactInfoDto);
            return Ok(updatedContactInfo);
        }
    }
}
